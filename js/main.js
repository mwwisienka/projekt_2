$(window).load(function() {
    // The slider being synced must be initialized first
    $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 145,
        itemMargin: 3,
        asNavFor: '#slider'
    });

    $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
    });
});


$( window ).resize(function() {
    if($( window ).width() >= 769){
        imgAnimate();
    }else{
        $( "div.img-interaction" ).unbind('mouseover mouseout');
    }
});

var imgAnimate = function(){
    $( "div.img-interaction" )
        .mouseover(function() {
            $(this).addClass('outline-white');
            $(this).find("img").addClass('shadow_ack');
            $(this).find("span").css('top','39%');
        })
        .mouseout(function() {
            $(this).removeClass('outline-white');
            $(this).find("img").removeClass('shadow_ack');
            $(this).find("span").css('top','42%');
        });
} ;
$(document).ready(function() {
    if($( window ).width() >= 769){
        imgAnimate();
    }

});